FROM ubuntu:latest
MAINTAINER Konstantin Morozov <morozov-kst@yandex.ru>
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install -y \
	build-essential make autoconf cmake git pkg-config \
	automake libtool curl g++ unzip \
	gcc-10 g++-10 \
	clang-10 clang++-10 \
	&& apt-get clean

RUN update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-10 100 --slave /usr/bin/g++ g++ /usr/bin/g++-10 --slave /usr/bin/gcov gcov /usr/bin/gcov-10

RUN apt-get update && \
    apt-get install -y \
    	libboost-all-dev -f \
	qt5-default \
	postgresql \
	libpq-dev \
	libgtest-dev \
	google-mock \
	cppcheck \
	libsqlite3-dev \
	&& apt-get clean

# install protobuf first, then grpc
ENV GRPC_RELEASE_TAG v1.34.0
ENV CC /usr/bin/clang-10
ENV CXX /usr/bin/clang++-10
RUN git clone -b v1.34.0 https://github.com/grpc/grpc && \
	cd grpc && \
 	git submodule update --init --recursive && \
 	mkdir -p cmake/build && \
 	cd third_party/protobuf && \
	./autogen.sh && \
	./configure && \
	make -j 4 && \
	make check && \
	make install && \
 	cd ../../cmake/build && \
 	cmake ../.. -DgRPC_INSTALL=ON          \
              -DCMAKE_BUILD_TYPE=Release       \
              -DgRPC_BUILD_TESTS=OFF 		 \
              -DgRPC_ABSL_PROVIDER=module     \
              -DgRPC_CARES_PROVIDER=module    \
              -DgRPC_PROTOBUF_PROVIDER=module \
              -DgRPC_RE2_PROVIDER=module      \
              -DgRPC_SSL_PROVIDER=module      \
              -DgRPC_ZLIB_PROVIDER=module	&& \
	make && \
	make install
