#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "MessageWidget/messagewidget.h"

MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent)
  , ui(new Ui::MainWindow)
  , channels_()
{
  ui->setupUi(this);

  connect(ui->lwChannels, &QListWidget::itemSelectionChanged, this, &MainWindow::channel_selected);
  connect(ui->bJoin, &QPushButton::clicked, this, &MainWindow::join_clicked);
  connect(ui->bSend, &QPushButton::clicked, this, &MainWindow::send_clicked);
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::set_login(const std::string &login)
{
  login_ = login;
}

void MainWindow::channel_received(const std::string& channel, messages_ptr history)
{
  add_channel({channel, 1, 1, history});
}

void MainWindow::channel_selected()
{
  const auto item = ui->lwChannels->currentItem();
  const auto widget = ui->lwChannels->itemWidget(item);
  const auto channel_widget = qobject_cast<ChannelWidget*>(widget);

  ui->lChannelName->setText(channel_widget->channel_name());

  clear_chat();

  for (const auto& msg : *channel_widget->history()) {
    add_message(msg);
  }
}

void MainWindow::join_clicked()
{
  const auto channel_name = ui->leSearch->text().toStdString();

  if (channel_name.empty())
    return;

  ui->leSearch->clear();
  emit join_to_channel(channel_name);
}

void MainWindow::send_clicked()
{
  const auto text = ui->teMessage->toPlainText().toStdString();

  if (text.empty())
    return;

  msg_text_t msg {
    login_,                                 // Author
    text,                                   // Text
    ui->lChannelName->text().toStdString(), // Channel name
    get_current_DateTime()                  // Timestamp
  };

  emit send_message(msg);

  ui->teMessage->clear();
}

void MainWindow::joined(const std::string& channel_name, messages_ptr history)
{
  add_channel({channel_name, 1, 1, history});
}

void MainWindow::message_received(const msg_text_t& msg)
{
  const auto it = channels_.find(msg.channel_name);
  it->second->update_displayed_msg();

  if (ui->lChannelName->text().toStdString() == it->first) {
    add_message(msg);
  }
}

void MainWindow::clear_chat()
{
  for (int i = 0; i < ui->lwMessages->count(); i++) {
    const auto item = ui->lwMessages->item(i);
    const auto widget = ui->lwMessages->itemWidget(item);
    delete widget;
  }
  ui->lwMessages->clear();
}

void MainWindow::add_channel(chat_t datachat)
{
  channels_[datachat.name] = std::make_unique<ChannelWidget>();
  channels_[datachat.name]->set_channel(datachat);

  auto item = new QListWidgetItem(ui->lwChannels);
  item->setSizeHint(channels_[datachat.name]->sizeHint());

  ui->lwChannels->setItemWidget(item, channels_[datachat.name].get());
}

void MainWindow::add_message(msg_text_t msg)
{
  auto widget = new MessageWidget();
  widget->set_message(msg);

  auto item = new QListWidgetItem(ui->lwMessages);
  item->setSizeHint(widget->sizeHint());

  ui->lwMessages->setItemWidget(item, widget);
}

