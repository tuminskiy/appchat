#pragma once

#include <QMainWindow>

#include "client/client.h"
#include "ChannelWidget/channelwidget.h"

namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

  void set_login(const std::string& login);

signals:
  void join_to_channel(const std::string&);
  void send_message(const msg_text_t&);

public slots:
  void channel_received(const std::string& channel, messages_ptr history);
  void message_received(const msg_text_t& msg);
  void joined(const std::string& channel_name, messages_ptr history);

private slots:
  void channel_selected();
  void join_clicked();
  void send_clicked();

private:
  Ui::MainWindow *ui;
  std::string login_;
  std::unordered_map<std::string, std::unique_ptr<ChannelWidget>> channels_;

  void clear_chat();

  void add_channel(chat_t);
  void add_message(msg_text_t);
};

