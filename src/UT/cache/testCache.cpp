#include "gtest/gtest.h"

#include "cache/sqlitecache.h"

#include <filesystem>
#include <memory>

namespace fs = std::filesystem;

class TestCacheConfig : public ::testing::Test
{
protected:
  Storage::SqliteConfig config_;
  std::unique_ptr<Storage::SqliteCache> cache_;

  static constexpr auto author = "TestAuthor";
  static constexpr auto channel_name = "TestChannelName";
  static constexpr auto text = "TestText";  
};


class TestCache : public TestCacheConfig
{
public:
  void SetUp() override
  {
    config_.folder_path = std::string(fs::current_path()) + "/Temp/";
    config_.connection_string = "file://" + config_.folder_path + "TestCache.db";

    cache_ = std::make_unique<Storage::SqliteCache>(config_);
  }

  void TearDown() override
  {
    fs::remove(config_.folder_path + "TestCache.db");
    fs::remove_all(config_.folder_path);
  }
};

TEST_F(TestCache, test_save_load)
{
  const msg_text_t msg{author, text, channel_name, DateTime{Time(20, 27, 21), Date()}};

  EXPECT_EQ(cache_->load(channel_name).empty(), true);

  EXPECT_EQ(cache_->save(channel_name, msg), true);

  const auto history = cache_->load(channel_name);

  EXPECT_EQ(history.empty(), false);

  EXPECT_EQ(history.front(), msg);
};
