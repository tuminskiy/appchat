#ifndef CONFIG_WRAPPER_H
#define CONFIG_WRAPPER_H

#include "gtest/gtest.h"
#include "storage/sqlitedatabase.h"
#include "protocol/command_table.h"

class DbConfig : public ::testing::Test {
protected:
    Storage::DatabaseConfiguration db_config;
    Storage::database_ptr m_db;  

    static constexpr auto author = "author_msg";
    static constexpr auto channel_name = "test_name_channel";
    static constexpr auto text = "test messages"; 
};


#endif