#pragma once

#include <string>

namespace Storage
{

struct ConnectSettings {
    std::string user = "worker";
    std::string password = "123";
    std::string host = "127.0.0.1";
    std::string db = "chat";
};

} // namespace Storage
