#pragma once

#include <string>
#include <memory>
#include "database.h"
#include <postgresql/libpq-fe.h>

namespace Storage {

class WrapperPQ : public Database
{
public:
    WrapperPQ() = default;

    bool connect(const ConnectSettings& setting = ConnectSettings()) override;

    bool add_logins(std::string login, std::string password) override;

    identifier_t get_loginid(std::string login) const override;

    identifier_t check_client(std::string login, std::string password) const override;

    bool save_text_message(const msg_text_t& message) override;

    std::deque<msg_text_t> get_history(const std::string& a_channel_name) const override;

    std::deque<std::string> get_channels(identifier_t client_id) const override;

    virtual ~WrapperPQ();
  
private:
    PGconn *conn;
    bool isConnect = false;

    std::string get_login(const std::string& client_id) const;
};

} // namespace Storage
