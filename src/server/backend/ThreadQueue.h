#pragma once

#include <thread>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <queue>
#include <future>

namespace Thread {

using task_type = std::future<void>;

class Queue {
public:
    void start() {
        threads.emplace_back(&Queue::worker, this);
        threads.emplace_back(&Queue::worker, this);
        quit = false;
    }

    void add_task(task_type&& a_task) {
        std::lock_guard<std::mutex> lk(cv_m);
        m_thread_queue.emplace(std::move(a_task));
    }

    void notify() {
        cv.notify_all();
    }

    ~Queue() {
        quit = true;
        cv.notify_all();

        std::for_each(threads.begin(), threads.end(), std::mem_fn(&std::thread::join));
    }
private:
    std::condition_variable cv;
    std::mutex cv_m;
    std::queue<task_type> m_thread_queue;
    std::atomic_bool quit;

    std::vector<std::thread> threads;

    void worker() {
        while(!quit) {
            std::unique_lock<std::mutex> lk(cv_m);

            cv.wait(lk, [this]() {
                return !m_thread_queue.empty() || quit;
            });

            if(!m_thread_queue.empty()) {
                auto future = std::move(m_thread_queue.front());

                m_thread_queue.pop();
                lk.unlock();

                future.get();

            }
        }
    };
};

}