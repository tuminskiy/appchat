#include "backend/channel/channels_manager.h"

ChannelsManager::ChannelsManager(const Config::ChannelsManagerConfig& a_config) 
    : m_config(a_config)
{
    BOOST_LOG_TRIVIAL(info) << "create ChannelsManager";
}

bool ChannelsManager::join(const Config::JoinChannelManagerConfig &a_config) {
    bool flag_result = true;
    BOOST_LOG_TRIVIAL(info) << "ChannelsManager::join";

    if (auto it_cl = client_in_rooms.find(a_config.client_id); it_cl == client_in_rooms.end()) {
        auto [nit, flag] = client_in_rooms.emplace(a_config.client_id, std::deque<std::string>());
        if (flag) {
            BOOST_LOG_TRIVIAL(info) << "add channel for new client_id";
            nit->second.push_back(a_config.channel_name);
        }
    }
    else {
        BOOST_LOG_TRIVIAL(info) << "add channel for old client_id";
        it_cl->second.push_back(a_config.channel_name);
    }

    if (auto it=channels.find(a_config.channel_name); it!=channels.end()) {
        BOOST_LOG_TRIVIAL(info) << "channel found";
        Config::JoinChannelConfig config;
        config.client_id = a_config.client_id;
        config.send_front_callback = a_config.send_front_callback;

        it->second->join_channel(config);
    }
    else {
        BOOST_LOG_TRIVIAL(info) << "channel not found";

        Config::CreateChannelConfig config;
        config.channel_name = a_config.channel_name; 
        config.db = m_config.db; 
        
        auto [new_it, flag] = channels.emplace(a_config.channel_name, 
            std::make_shared<Channel>(config));

        if (!flag) {
            BOOST_LOG_TRIVIAL(error) << "Non create";
            flag_result = false;
            return flag_result;
        }
        Config::JoinChannelConfig channel_conf;
        channel_conf.client_id = a_config.client_id;
        channel_conf.send_front_callback = a_config.send_front_callback;

        new_it->second->join_channel(channel_conf);
    }

    clientid_to_login.try_emplace(a_config.client_id, a_config.login);

    return flag_result;
}

void ChannelsManager::send_to_channel(const msg_text_t& a_msg) {
    BOOST_LOG_TRIVIAL(info) << "send to channel=" << a_msg.channel_name;
    if (auto it=channels.find(a_msg.channel_name); it!=channels.end()) {
        it->second->notification(a_msg);
    }
    else {
        BOOST_LOG_TRIVIAL(info) << "no found channel=" << a_msg.channel_name;
    }
}

void ChannelsManager::leave_from_all_channels(identifier_t a_client_id) {
    BOOST_LOG_TRIVIAL(info) << "leave_from_all_channels() client_id=" << a_client_id;
    if (auto it = client_in_rooms.find(a_client_id); it != client_in_rooms.end()) {
        for(const auto& room_id : it->second) {
            if (auto it_room=channels.find(room_id); it_room!=channels.end()) {
                it_room->second->leave(a_client_id);
            }
        }
        client_in_rooms.erase(it);
    }

    clientid_to_login.erase(a_client_id);
}

void ChannelsManager::leave(identifier_t client_id, const std::string& a_channel_name) {
    BOOST_LOG_TRIVIAL(info) << "ChannelsManager::leave() client_id=" << client_id << ", from channel=" << a_channel_name;

    auto it_client = client_in_rooms.find(client_id);
    if (it_client == client_in_rooms.end()) {
        BOOST_LOG_TRIVIAL(info) << "not found client_id=" << client_id << ", in channels";
        return;
    }
    if (auto it=channels.find(a_channel_name); it!=channels.end()) {
        it->second->leave(client_id);

        client_in_rooms.erase(it_client);
        clientid_to_login.erase(client_id);

        BOOST_LOG_TRIVIAL(info) << "client_id=" << client_id
                                << " is leave from channel=" << a_channel_name;
    }
    else {
        BOOST_LOG_TRIVIAL(error) << "no found channel" << a_channel_name;
    }

}

std::optional<channel_ptr_t> ChannelsManager::get_channel(const std::string& channel_name) {
    if (auto it = channels.find(channel_name); it!=channels.end()) {
        return it->second;
    }

    return std::nullopt;
}

std::deque<std::string> ChannelsManager::get_client_rooms(identifier_t a_client_id) const {
    if (auto it = client_in_rooms.find(a_client_id); it!=client_in_rooms.end()) {
        return it->second;
    }

    return std::deque<std::string>();
}

std::string ChannelsManager::get_login(identifier_t a_client_id) const {
    if (auto it = clientid_to_login.find(a_client_id); it!=clientid_to_login.end()) {
        return it->second;
    }

    return std::string();
}

void ChannelsManager::send_history(const Config::HistoryChannelManagerConfig &a_config) {
// void ChannelsManager::send_history(subscriber_ptr subscriber, const std::string& channel_name) {
    // const auto client_id = subscriber->get_client_id();

    // if (client_in_rooms.find(client_id) == client_in_rooms.end()) {
    //     BOOST_LOG_TRIVIAL(error) << "not found client_id = " << client_id << " in channels";
    //     return; 
    // }
    auto it_channel = channels.find(a_config.channel_name);
    if ( it_channel != channels.end() ) {
        Config::HistoryChannelConfig config;
        config.since = a_config.since;
        config.send_history_callback = a_config.send_history_callback;
        // @todo get history and send here
        it_channel->second->send_history(config);

    } else {
        BOOST_LOG_TRIVIAL(error) << "not found channel = " << a_config.channel_name;
        return; 
    }

    // auto it_channel = channels.find(channel_name);

    // if (it_channel == channels.end()) {
    //     BOOST_LOG_TRIVIAL(error) << "not found channel = " << channel_name;
    //     return;
    // }
}
