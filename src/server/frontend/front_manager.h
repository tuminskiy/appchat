//#ifndef FRONTMANAGER_H
//#define FRONTMANAGER_H

//#include "frontend.h"

///**
// * @brief FrontManager
// * @details Manager to control connections from clinets. Use ObjectPool.
// */
//class FrontManager
//{
//public:
//    FrontManager(std::shared_ptr<boost::asio::thread_pool> a_thread_pool):
//        thread_pool(a_thread_pool),
//    {
//        BOOST_LOG_TRIVIAL(info) << "create FrontManager";
//    }
//    FrontManager(const FrontManager&) = delete;

//    /**
//     * @brief get new or old connection for new client
//     * @param _socket
//     */
//    connection_ptr get_connection(boost::asio::ip::tcp::socket&& _socket);

//    void close_all() {
//        for (auto& value_con: pool_connections) {
//            value_con->set_busy(false);
//        }
//        pool_connections.clear();
//    }

//    ~FrontManager() {
//        close_all();
//    }
//private:
//    std::shared_ptr<boost::asio::thread_pool> thread_pool;
//    std::vector<connection_ptr> pool_connections;

//    /**
//     * @brief print all connections
//     * */
//    void print_pool() const noexcept;
//};

//#endif // FRONTMANAGER_H
