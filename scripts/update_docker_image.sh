#!/bin/bash

apt  install docker.io -y
docker build -t kstmorozov/appchat-env .
docker login
docker push kstmorozov/appchat-env:latest

